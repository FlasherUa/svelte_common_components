/// <reference path="FormBuilder.d.ts" />
import { iFBFormCfg, tID, iFormError, tFBFormStatuses } from "./FormBuilder";
import EventsClass from "../Modules/Helpers/EventsClass";
import { SvelteComponent } from "svelte/internal";
import { I_FormData, I_FormItem } from "./Validate/@types";
export default class FormBuilder extends EventsClass {
    _cfg: iFBFormCfg;
    _dataId: tID;
    _utils: any;
    id: tID;
    name: string;
    cfg: iFBFormCfg;
    items: {
        key?: I_FormItem;
    };
    errors: Array<iFormError>;
    _component: SvelteComponent;
    _isAddAction: boolean;
    data: any;
    static forms_counter: number;
    constructor(cfg: iFBFormCfg, component: SvelteComponent, id?: tID);
    addItems(items: any): void;
    submit(): Promise<void>;
    _setItemsValues(silent?: boolean): void;
    _getItemsValues(validate?: boolean): Promise<I_FormData>;
    _valueHelper(value: any): any;
    /**
     *
     * @param {undefined} status
     * @param {undefined} text
     */
    _setStatus(status: tFBFormStatuses, text: string): void;
}
