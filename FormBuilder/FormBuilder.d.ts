/// <reference path="../@types/form_types.d.ts" />

import FormItem from "./FormItemFactory.v3.Svelte"

export interface iFBInput extends FormItem {
    setValue (value:any):Promise<any>|void
    getValue ():Promise<any>|any
    validate():boolean

}

export class iFBInputView {
    title: string
    id: tID
    name: string
    description: string
    classList: string
    attributes: string
}

export type tFBFormStatuses = "loading"| "submitted"|"isDirty"|"loaded"| "ok" | "error"
export type tFBInputStatuses = "loading"|  "ok" | "error"
export interface iFBFormCfg {
    name:string
    _id:string
    extends:string
    id:string|number
    saveDataFunction:string
    actionType:"add"|"edit"
    events:Array<any>
}

export type tID= number | string

export interface iFBFormInputCfg extends iFBFormCfg{
    notFound:boolean
}

export interface iFormError {

}
