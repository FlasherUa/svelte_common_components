export var InputState_state;
(function (InputState_state) {
    InputState_state["Ok"] = "Ok";
    InputState_state["Error"] = "Error";
    InputState_state["Warning"] = "Warning";
})(InputState_state || (InputState_state = {}));
export default class InputState {
    constructor(message = "", state = InputState_state.Ok, code = "") {
        this.state = InputState_state.Ok;
        this.message = "";
        this.code = "";
        this.message = message;
        this.code = code;
        this.state = state;
    }
}
