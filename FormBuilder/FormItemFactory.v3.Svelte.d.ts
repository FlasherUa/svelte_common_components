import { iFBFormCfg, iFBFormInputCfg, iFormError, tFBInputStatuses, tID } from "./FormBuilder";
import FormBuilder from "./FormBuilder.v.3.Svelte";
import EventsClass from "../Modules/Helpers/EventsClass";
import { SvelteComponent } from "svelte/internal";
export default class FormItem extends EventsClass {
    _cfg: iFBFormCfg;
    _dataId: tID;
    _utils: any;
    id: tID;
    name: string;
    cfg: iFBFormCfg;
    errors: Array<iFormError>;
    _component: SvelteComponent;
    form: FormBuilder;
    _hasError: boolean;
    _containerCfg: iFBFormInputCfg;
    parent: FormBuilder;
    constructor(cfg: iFBFormCfg, parent: FormBuilder);
    /**
     *
     */
    validate(): boolean;
    /**
     * setValue proxy
     * @param {*} value
     * @param {*} silent
     */
    /**
     *

     */
    _addError(): void;
    /**
     *

     */
    _removeError(): void;
    /**
     *

     */
    render(): void;
    getValue(): void;
    /**
     *
     * @param {Object} data
     */
    /**
     * Proxy method for theme set status
     * @param {Object} text
     * @param {Object} status
     */
    setStatus(status: tFBInputStatuses, text?: string): void;
}
