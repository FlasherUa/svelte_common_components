/// <reference path="./FormBuilder.d.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import formUtils from "./formUtils";
import EventsClass from "../Modules/Helpers/EventsClass";
//@ts-ignores
import { addslashes } from "../Modules/Helpers/Data/_";
import { InputState_state } from "./InputState";
const f = null;
export default class FormBuilder extends EventsClass {
    constructor(cfg, component, id = null) {
        super();
        this._isAddAction = false;
        // cfg = isString(cfg) ? {name: cfg} : cfg
        this.cfg = cfg; //intial cfg
        this._dataId = id;
        this._utils = formUtils;
        this._component = component;
        this.id = cfg.id ? cfg.id : "form_" + FormBuilder.forms_counter;
        FormBuilder.forms_counter++;
        this.name = cfg.name;
        //subscribe all events
        if (cfg.events) {
            for (var name in cfg.events) {
                const evtCfg = cfg.events[name];
                const handler = new Function(evtCfg.sourceCode);
                this.on(name, handler);
            }
        }
        //add value property
        Object.defineProperty(this, "status", {
            set: function (value) {
                return this._setStatus(value);
            }
        });
        this.trigger("onInit");
    }
    addItems(items) {
        this.items = items;
    }
    submit() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this._getItemsValues();
            if (!data)
                return;
            /**
             * Add action patch
             *
             */
            //@ts-ignore
            const pk_name = this.cfg.primaryKey, 
            //@ts-ignore
            url = this._isAddAction ? this.cfg.addDataURL : this.cfg.saveDataURL;
            //@ts-ignore
            if (this._isAddAction) {
                delete data[pk_name];
            }
            let query = "";
            //Todo extebd this to grapQl version
            this.data = data;
            this.trigger("beforeSendData");
            for (let k in this.data) {
                let item = this.data[k];
                if (typeof item === "undefined" || item === null)
                    continue;
                if (item && item.replace)
                    item = addslashes(item);
                if (typeof item === "number") {
                    query += " " + k + ":" + item + " ";
                }
                else {
                    query += " " + k + ":\"" + item + "\" ";
                }
            }
            if (this.cfg.saveDataFunction) {
            }
            query = url.replace("%data%", query);
            this._utils.fetcher(query).then((result) => {
                if (result.errors) {
                    this._setStatus("error", result.errors[0].message);
                }
                else {
                    this.trigger("onSubmitted");
                }
            });
        });
    }
    _setItemsValues(silent = false) {
        silent || (silent = false);
        let isDirty = false;
        if (this.data && this.items && this.items.length > 0) {
            isDirty = this.items.map((item) => {
                const itemName = item.name, val = this.data[itemName];
                const _isDirty = item.setValue(val);
                return _isDirty;
            });
        }
        if (!silent)
            this.trigger("onChanged", isDirty);
    }
    _getItemsValues(validate = true) {
        return __awaiter(this, void 0, void 0, function* () {
            //collect items data
            //todo validate
            const data = {}, errors = [], itemsKeys = Object.keys(this.items);
            if (itemsKeys && itemsKeys.length > 0) {
                //check errors
                const errors = itemsKeys.find((itemName) => {
                    //@ts-ignore
                    const item = this.items[itemName];
                    return item.state.state === InputState_state.Error;
                });
                if (errors)
                    return null;
                const data = {};
                yield asyncForEach(itemsKeys, (itemName) => __awaiter(this, void 0, void 0, function* () {
                    //@ts-ignore
                    const item = this.items[itemName];
                    let value = yield this._valueHelper(item.value);
                    data[itemName] = value;
                }));
                return data;
            }
        });
    }
    _valueHelper(value) {
        if (typeof value === "function") {
            return value();
        }
        else
            return new Promise((resolve, reject) => resolve(value));
    }
    /**
     *
     * @param {undefined} status
     * @param {undefined} text
     */
    _setStatus(status, text) {
        /*  var statusEl = this.el.getElementsByClassName("status")
//[0]
          let html = ""
          switch (status) {
              case "loading":
                  html = " <span class='forms-submit-status loading'></span> "
                  break;
              case "submitted":
                  html = " <span class='forms-submit-status submitted'></span> "
                  break;
              case "isDirty":
                  html = " <span class='forms-submit-status dirty'></span> "
                  break;
              case "loaded":
              case "ok":
                  html = " <span class='forms-submit-status ok'></span> "
                  break
              case "error":
              default:
                  html = " <span class='forms-submit-status error'>" + text + "</span> "
          }
          if (statusEl.length > 0) {
              //statusEl.forEach((el) => el.innerHTML = html)
              statusEl[0].innerHTML = html
          }
          ;
*/
    }
}
FormBuilder.forms_counter = 0;
function asyncForEach(array, callback) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let index = 0; index < array.length; index++) {
            yield callback(array[index], index, array);
        }
    });
}
