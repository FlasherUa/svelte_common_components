import {IFormItemError} from "../../Common/@types/form_types.d";
import Validate from "./Validate/Validate";
import InputState from "./InputState";

export interface ValidateCfg {
    maxLen: number,
    minLen: number
}

export namespace FormItem {
    /**
     * validate string
     * @param value
     * @param cfg
     */
    export const validate = (value: any, cfg: ValidateCfg): IFormItemError[] => {
        const err = new Errors()
        if (cfg.minLen && cfg.minLen > 0 && value.length == 0) {
            if (value.length === 0) err.addError("You left this box empty")
            else {
                err.addError(cfg.minLen + " chars minimum at this box")
            }
        }
        if (cfg.maxLen && cfg.maxLen - value.length < 0) {
            const message = (value.length - cfg.maxLen) + " more chars than possible"
            err.addError(message)
        }
        //if valid - show chars left
        if (!err.get()) {
            const message = (cfg.maxLen - value.length) + " chars left"
            err.addError(message, 0)
        }
        return err.get()
    }
    export const getState = (value: any, validateCfg: any): InputState => {
        if (!validateCfg) return
        const r = Validate(value, validateCfg)
        return r
    }
}

class Errors {
    private out: IFormItemError[] = []

    addError(message: string, code: number = 1) {
        this.out.push({
            message, code
        })
    }

    get() {
        return this.out
    }
}
