//import forms_all_list from './forms_all_list'
import deepExtend from "../../../../dashboard/src/components/jsModules/data/deepExtend";
export var resolveFormCfg = function (target) {
    //extends obj
    if (target.name == "_defaultFormCfg")
        return target; //nom oew exteds
    let _sourceName, _sourceObj; //= target.extends? target.extends :  "_defaultFormCfg"
    if (target.extends && target.extends != "_defaultFormCfg") {
        //get  extended parent obj
        _sourceObj = resolveFormCfg(forms_all_list[target.extends]);
    }
    else {
        _sourceName = "_defaultFormCfg";
        _sourceObj = forms_all_list[_sourceName];
    }
    //do extend
    let rsult = deepExtend({}, _sourceObj, target);
    return rsult;
};
/**
 *
 * @param {undefined} itemCfg
 * @param {undefined} startFormName
 * @param {undefined} defaultItemName
 */
export var resolveItemCfg = function (itemCfg, startFormName, defaultItemName = null) {
    defaultItemName || (defaultItemName = "_default");
    //extends obj
    //const all=forms_all_list["_defaultItemsCfg"].itemsByName
    if (!itemCfg.notFound && itemCfg.name == defaultItemName)
        return itemCfg; //nom oew exteds
    let _sourceName, _sourceObj; //= itemCfg.extends? itemCfg.extends :  "_defaultFormCfg"
    if (itemCfg.notFound) {
        //fake input config
        _sourceObj = resolveItemCfg(findItemCfgInParents(itemCfg.name, startFormName), startFormName);
    }
    else {
        //full input config
        if (itemCfg.extends && itemCfg.extends != defaultItemName) {
            //search extended parent obj
            _sourceObj = resolveItemCfg(findItemCfgInParents(itemCfg.extends, startFormName), startFormName);
        }
        else {
            _sourceName = defaultItemName;
            _sourceObj = resolveItemCfg(findItemCfgInParents(_sourceName, startFormName), startFormName);
        }
    }
    //do extend
    let rsult = deepExtend({}, _sourceObj, itemCfg);
    return rsult;
};
/**
 * searches item cfg by name in parents forms
 * @param {undefined} itemName
 * @param {undefined} parentName
 */
var findItemCfgInParents = function (itemName, parentName) {
    let form = parentName, itemCfg, prev;
    while (!itemCfg) {
        const formCfg = forms_all_list[form];
        itemCfg = formCfg.itemsByName[itemName];
        if (itemCfg)
            break;
        form = formCfg.extends;
        if (!form) {
            if (prev === "_defaultFormCfg")
                throw ("Input cfg not found " + parentName + ":" + itemName);
            form = "_defaultFormCfg";
        }
        prev = form;
    }
    return itemCfg;
};
