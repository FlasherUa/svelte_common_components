/// <reference path="./FormBuilder.d.ts" />

import {iFBFormCfg, iFBInput, iFBFormInputCfg, tID, iFormError, tFBFormStatuses} from "./FormBuilder";
import formUtils from "./formUtils";
import FormItem from "./FormItemFactory.v3.Svelte"

import FormItemFactory from './FormItemFactory.v3.Svelte'

import EventsClass from "../Modules/Helpers/EventsClass";
import {SvelteComponent} from "svelte/internal";

import {resolveFormCfg} from "./cfgHelper"
//@ts-ignores
import ff, {addslashes, addslashes as ee} from "../Modules/Helpers/Data/_";
import {TFormState} from "../@types/form_types";
import {I_FormData, I_FormItem} from "./Validate/@types";
import {InputState_state} from "./InputState";


const f: TFormState = null

export default class FormBuilder extends EventsClass {

    _cfg: iFBFormCfg
    _dataId: tID
    _utils: any
    id: tID
    name: string
    cfg: iFBFormCfg
    items: { key?: I_FormItem }
    errors: Array<iFormError>
    _component: SvelteComponent
    _isAddAction: boolean = false
    data: any
    static forms_counter = 0


    constructor(cfg: iFBFormCfg, component: SvelteComponent, id: tID = null) {
        super()
        // cfg = isString(cfg) ? {name: cfg} : cfg
        this.cfg = cfg//intial cfg
        this._dataId = id
        this._utils = formUtils
        this._component = component

        this.id = cfg.id ? cfg.id : "form_" + FormBuilder.forms_counter
        FormBuilder.forms_counter++

        this.name = cfg.name


        //subscribe all events
        if (cfg.events) {
            for (var name in cfg.events) {
                const evtCfg = cfg.events[name]
                const handler = new Function(evtCfg.sourceCode)
                this.on(name, handler)
            }
        }

        //add value property
        Object.defineProperty(this, "status", {
            set: function (value) {
                return this._setStatus(value)
            }
        });

        this.trigger("onInit");

    }

    addItems(items: any) {
        this.items = items
    }

    async submit() {

      const data:I_FormData= await this._getItemsValues()
         if (!data) return
            /**
             * Add action patch
             *
             */
                //@ts-ignore
            const pk_name = this.cfg.primaryKey,
                //@ts-ignore
                url = this._isAddAction ? this.cfg.addDataURL : this.cfg.saveDataURL
            //@ts-ignore
            if (this._isAddAction) {
                delete data[pk_name]
            }
            let query = ""
            //Todo extebd this to grapQl version

            this.data=data
            this.trigger("beforeSendData" );

            for (let k in  this.data) {
                let item =  this.data[k]
                if (typeof item === "undefined" || item === null) continue
                if (item && item.replace) item = addslashes(item)
                if (typeof item === "number") {
                    query += " " + k + ":" + item + " "
                } else {
                    query += " " + k + ":\"" + item + "\" "
                }
            }


            if (  this.cfg.saveDataFunction  ) {

            }


            query =    url.replace("%data%", query)
            this._utils.fetcher(query).then((result: any) => {
                if (result.errors) {
                    this._setStatus("error", result.errors[0].message)
                } else {
                    this.trigger("onSubmitted")
                }
            })


    }

    _setItemsValues(silent = false) {
        silent || (silent = false)
        let isDirty: any = false
        if (this.data && this.items && this.items.length > 0) {
            isDirty = this.items.map((item) => {
                const itemName: string = item.name,
                    val = this.data[itemName]
                const _isDirty = item.setValue(val)
                return _isDirty
            })
        }
        if (!silent) this.trigger("onChanged", isDirty);

    }

     async _getItemsValues(validate = true): Promise<I_FormData> {
        //collect items data
        //todo validate
        const data = {}, errors: Array<iFormError> = [],
            itemsKeys: string[] = Object.keys(this.items)
        if (itemsKeys && itemsKeys.length > 0) {
            //check errors
            const errors = itemsKeys.find((itemName: string) => {
                //@ts-ignore
                const item: I_FormItem = this.items[itemName]
                return item.state.state === InputState_state.Error
            })
            if (errors) return null
            const data:any={}

            await  asyncForEach ( itemsKeys ,async (itemName: string) => {
                //@ts-ignore
                const item: I_FormItem = this.items[itemName]
                let value=await this._valueHelper(item.value)
                data[itemName]=value
            })
            return data
        }
    }
        _valueHelper (value:any) {
           if (typeof value  === "function"  ){
               return   value()
            }else return  new Promise ((resolve,reject)=>resolve(value))
        }

    /**
     *
     * @param {undefined} status
     * @param {undefined} text
     */

    _setStatus(status: tFBFormStatuses, text: string) {
        /*  var statusEl = this.el.getElementsByClassName("status")
//[0]
          let html = ""
          switch (status) {
              case "loading":
                  html = " <span class='forms-submit-status loading'></span> "
                  break;
              case "submitted":
                  html = " <span class='forms-submit-status submitted'></span> "
                  break;
              case "isDirty":
                  html = " <span class='forms-submit-status dirty'></span> "
                  break;
              case "loaded":
              case "ok":
                  html = " <span class='forms-submit-status ok'></span> "
                  break
              case "error":
              default:
                  html = " <span class='forms-submit-status error'>" + text + "</span> "
          }
          if (statusEl.length > 0) {
              //statusEl.forEach((el) => el.innerHTML = html)
              statusEl[0].innerHTML = html
          }
          ;
*/


    }
}


async function asyncForEach(array:any[], callback:Function) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
