import Validate from "./Validate/Validate";
export var FormItem;
(function (FormItem) {
    /**
     * validate string
     * @param value
     * @param cfg
     */
    FormItem.validate = (value, cfg) => {
        const err = new Errors();
        if (cfg.minLen && cfg.minLen > 0 && value.length == 0) {
            if (value.length === 0)
                err.addError("You left this box empty");
            else {
                err.addError(cfg.minLen + " chars minimum at this box");
            }
        }
        if (cfg.maxLen && cfg.maxLen - value.length < 0) {
            const message = (value.length - cfg.maxLen) + " more chars than possible";
            err.addError(message);
        }
        //if valid - show chars left
        if (!err.get()) {
            const message = (cfg.maxLen - value.length) + " chars left";
            err.addError(message, 0);
        }
        return err.get();
    };
    FormItem.getState = (value, validateCfg) => {
        if (!validateCfg)
            return;
        const r = Validate(value, validateCfg);
        return r;
    };
})(FormItem || (FormItem = {}));
class Errors {
    constructor() {
        this.out = [];
    }
    addError(message, code = 1) {
        this.out.push({
            message, code
        });
    }
    get() {
        return this.out;
    }
}
