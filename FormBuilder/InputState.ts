export  enum InputState_state { Ok="Ok", Error="Error", Warning="Warning"}

export interface I_InputState {
    state: InputState_state
    message: string
    code: string
}


export default class InputState implements I_InputState {
    state = InputState_state.Ok
    message = ""
    code = ""

    constructor(message = "", state: InputState_state= InputState_state.Ok, code: string = "") {
       this.message = message
       this.code = code
       this.state = state
    }
}
