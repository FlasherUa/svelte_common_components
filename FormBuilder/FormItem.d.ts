import { IFormItemError } from "../@types/form_types";
import InputState from "./InputState";
export interface ValidateCfg {
    maxLen: number;
    minLen: number;
}
export declare namespace FormItem {
    /**
     * validate string
     * @param value
     * @param cfg
     */
    const validate: (value: any, cfg: ValidateCfg) => IFormItemError[];
    const getState: (value: any, validateCfg: any) => InputState;
}
