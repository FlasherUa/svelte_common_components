import EventsClass from "../Modules/Helpers/EventsClass";
export default class FormItem extends EventsClass {
    constructor(cfg, parent) {
        super();
        this._hasError = false;
        this._cfg = cfg; //initial cfg
        this.form = parent;
        this.name = cfg.name;
        const item = this;
        //do extend form config
        if (!cfg.name) {
            throw ("No form name!");
        }
        //const resolved = resolveItemCfg(cfg, parent.name)
        const resolved = this.cfg = cfg;
        //Container
        /*   this._containerCfg = resolveItemCfg(
                {
                           name: resolved.container,
                           notFound: true
           }, parent.name, "_defaultInputContainer")
           // this._containerTpl = template(this._containerCfg.template)
   */
        //subscribe all events
        if (resolved.events) {
            for (var name in resolved.events) {
                const evtCfg = resolved.events[name];
                const handler = new Function(evtCfg.sourceCode);
                this.on(name, handler);
            }
        }
        //add El property
        Object.defineProperty(this, "el", {
            get: function () {
                if (this._el.id)
                    return this._el;
                const el = document.getElementById(this.id);
                //if (! el )   throw ("No element id `" + this.id+"` found. Did you put id to template? ")
                this._el = el ? el : {};
                return this._el;
            }
        });
        //add value property
        Object.defineProperty(this, "value", {
            get: function () {
                return this.getValue();
            },
            set: function (value) {
                return this.setValue(value);
            }
        });
        this.on("changed", () => this.parent.trigger("onChanged", this));
        this.trigger("onInit");
    }
    /**
     *
     */
    validate() {
        return true;
    }
    /**
     * setValue proxy
     * @param {*} value
     * @param {*} silent
     */
    /* setValue (value:any, silent:boolean=false) {
         if (!this._setValue && this.cfg.setValue) {
             this._setValue = new Function('value', this.cfg.setValue)
         }
         var oldValue = this.getValue()
         //this.trigger("onChanged", value)
         this._setValue(value)
         if (oldValue !== value) {
             if (!silent) this.trigger("onChanged", value)
         }
         ;

     }*/
    /*  getValue  () {
          if (!this._getValue && this.cfg.getValue) {
              this._getValue = new Function(this.cfg.getValue)
          }

          return this._getValue();

      }*/
    /**
     *

     */
    _addError() {
    }
    /**
     *

     */
    _removeError() {
    }
    /**
     *

     */
    render() {
        this.trigger("beforeRender");
        /*     const data = {
                 title: this.cfg.title,
                 id: this.id,
                 name: this.name,
                 description: this.cfg.description ? this.cfg.description : "",
                 "classList": this.cfg.classList ? this.cfg.classList : "",
                 attributes: this.cfg.attributes ? this.cfg.attributes : "",
                 // items:items_html
             }

             let input = this._tpl(data)

     //render container
             data.input = input
             let out = this._containerTpl(data)

     //output if element exists
             this.el.innerHTML = out
     //called after parent form is added to DOM
     //this.trigger("onRendered", out)

             return out;*/
    }
    getValue() {
    }
    /**
     *
     * @param {Object} data
     */
    /* var _tpl = function (data) {
         if (!this.__template) {
             this.__template = template(this.cfg.template)
         }

         return this.__template(data);

     }*/
    /**
     * Proxy method for theme set status
     * @param {Object} text
     * @param {Object} status
     */
    setStatus(status, text = "") {
        //@ts-ignore
        if (this._setStatus) {
            //@ts-ignore
            this._setStatus(status, text);
        }
    }
}
