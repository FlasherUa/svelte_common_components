export declare enum InputState_state {
    Ok = "Ok",
    Error = "Error",
    Warning = "Warning"
}
export interface I_InputState {
    state: InputState_state;
    message: string;
    code: string;
}
export default class InputState implements I_InputState {
    state: InputState_state;
    message: string;
    code: string;
    constructor(message?: string, state?: InputState_state, code?: string);
}
