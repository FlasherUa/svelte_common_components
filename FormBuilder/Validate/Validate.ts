
import {I_Validate_cfg, I_Validate_cfg_string} from "./@types";
import InputState from "../InputState";
import Validate_Sting from "./Validate_String"

export default (value: any, validateCfg: I_Validate_cfg): InputState => {
    //@ts-ignore
    validateCfg.type=typeof  validateCfg.maxLen!=="undefined" ? "string" : ""

    switch (validateCfg.type) {
        case "string" :
            return Validate_Sting(value,<I_Validate_cfg_string>validateCfg)
        default:
    }

    return new InputState()
}

