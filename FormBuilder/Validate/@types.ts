import {I_InputState} from "../InputState";

export interface I_Validate_cfg {
    foo?(value:any):TInputState
    type?:"string"|"number",
}


export interface I_Validate_cfg_string extends I_Validate_cfg {
    type?:"string"
    maxLen:number
    minLen:number
}
export type F_ValuePromise= ()=>Promise<any>

export interface I_FormItem {
    value:any|F_ValuePromise,
    state:I_InputState
}
export  interface I_FormData {
    [key: string]: any
}
