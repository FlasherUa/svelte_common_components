import { I_Validate_cfg_string } from "./@types";
import InputState from "../InputState";
export default function (value: string, validateCfg: I_Validate_cfg_string): InputState;
