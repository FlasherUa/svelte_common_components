import InputState from "../InputState";
import Validate_Sting from "./Validate_String";
export default (value, validateCfg) => {
    //@ts-ignore
    validateCfg.type = typeof validateCfg.maxLen !== "undefined" ? "string" : "";
    switch (validateCfg.type) {
        case "string":
            return Validate_Sting(value, validateCfg);
        default:
    }
    return new InputState();
};
