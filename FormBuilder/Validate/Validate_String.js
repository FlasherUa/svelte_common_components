import InputState, { InputState_state } from "../InputState";
var VALIDATE_STRING_CODES;
(function (VALIDATE_STRING_CODES) {
    VALIDATE_STRING_CODES[VALIDATE_STRING_CODES["T_EMPTY"] = 0] = "T_EMPTY";
})(VALIDATE_STRING_CODES || (VALIDATE_STRING_CODES = {}));
export default function (value, validateCfg) {
    if (typeof value === "undefined")
        return new InputState();
    if (validateCfg.minLen > 0) {
        if (value.length === 0) {
            return new InputState("You left this box empty", InputState_state.Error, "T_EMPTY");
        }
        else if (validateCfg.minLen > value.length) {
            return new InputState(validateCfg.minLen - value.length + " chars less than available", InputState_state.Error, "T_MINLEN");
        }
    }
    if (validateCfg.maxLen > 0) {
        const charsLeft = validateCfg.maxLen - value.length, validated = charsLeft >= 0;
        if (charsLeft < 0)
            return new InputState(Math.abs(charsLeft) + " chars more than available", InputState_state.Error, "T_MAXLEN");
        else {
            return new InputState(charsLeft + " chars left ", InputState_state.Ok, "T_MAXLEN_OK");
        }
    }
}
function error() {
}
