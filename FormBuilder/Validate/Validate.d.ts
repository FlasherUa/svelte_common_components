import { I_Validate_cfg } from "./@types";
import InputState from "../InputState";
declare const _default: (value: any, validateCfg: I_Validate_cfg) => InputState;
export default _default;
