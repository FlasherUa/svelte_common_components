import {getCurrentRoute, go} from "./router"

export namespace UsersRH {
    //add a user to route (position 2)
    export function add(id) {
        debugger
        const {route, routeParams} = getCurrentRoute()
        let uStr,
            users,
            ui, newRoute, search

        if (routeParams && routeParams.search) {
            search = routeParams.search
            uStr = search.users
            users = uStr === "" ? [] : getUsersFromStr(uStr)
            ui = users.indexOf(id)

        } else {
            users = []
        }

        if (ui == -1) {
            users.push(id)

        } else {
            //already in list
            return
        }
        //go new route
        const userStrNew = users.join(",")
        search || (search = {})
        search.users = userStrNew

        go(newRoute, null, true)
    }

    export function del(id) {

    }

    const getUsersFromStr = (str: string): string[] => str.split(",")
}