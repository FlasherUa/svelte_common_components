class History {

    constructor(callback) {
        /**
         * @public decoded hash without #
         * @type {string}
         */
        this.location =""
        this._getLocation()
        this._callback = callback
        //this._onpopstate.bind(this)
        window.addEventListener('popstate', ()=>this._onpopstate(), false);
    }

    _onpopstate() {
        this._getLocation()
        this._callback(this.location)
    }

    _getLocation() {
        const hash = window.location.hash
        this.location = hash.length > 0 ? decodeURI(hash.slice(1)) : ""
        return this.location
    }

    go(route, data) {
        window.history.pushState(data, 'Title', "#"+route)

        this._onpopstate(route)
        const e = new Event("pushState");
        e.arguments = data;
        window.dispatchEvent(e);
    }

    stop() {
    }
}

const factory = function (callback) {
    const history = new History(callback)
    return history
}
export default factory