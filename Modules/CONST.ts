import {IOption} from "../Components/Forms/formsInterfaces"

export const PeriodSize1:Array<IOption>=[
    {title:"Days", value:"1"},
    {title:"Weeks", value:"2"},
    {title:"Months", value:"3"},
]




export const months = new Array();
months[0] = "January";
months[1] = "February";
months[2] = "March";
months[3] = "April";
months[4] = "May";
months[5] = "June";
months[6] = "July";
months[7] = "August";
months[8] = "September";
months[9] = "October";
months[10] = "November";
months[11] = "December";

export const monthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_')