import fetcherGQL from "./graphQL/fetcherGraphQL"

export default function (api:string, type:"graphql"|"http"="graphql"):Function{

if (type==="graphql") {
    return (query:string, params:any = null, method:any = null, useCache:boolean = false)=>
        fetcherGQL(query, params ,
        method , useCache , api )
}
return void(0);
}