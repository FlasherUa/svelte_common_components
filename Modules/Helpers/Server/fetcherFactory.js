import fetcherGQL from "./graphQL/fetcherGraphQL";
export default function (api, type = "graphql") {
    if (type === "graphql") {
        return (query, params = null, method = null, useCache = false) => fetcherGQL(query, params, method, useCache, api);
    }
    return void (0);
}
