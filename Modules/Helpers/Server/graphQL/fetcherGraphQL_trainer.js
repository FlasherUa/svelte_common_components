import fetcher from "../fetcher/fetcher"
import getCAdata from "./getCAdata"
import cfg from "../../../../../../../trainer/src/config"
import {getSessionUser} from "../../userSession";
/**
 * Graphql wrapper
 * @param query
 * @param params
 * @param method
 * @param useCache
 */
export default function (query, params = null, method = null, useCache = false) {

    const auth = getSessionUser()._id

    //pack params to query
    return fetcher(cfg.api, {query: query}, null, auth).then((result) => {
        const o=   getCAdata(result)
        o.data={items:o.items,total:o.total}
        return o
    })
}
