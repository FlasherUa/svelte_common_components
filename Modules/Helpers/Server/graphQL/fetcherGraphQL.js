import fetcher from "../fetcher/fetcher"

import cfg from "../../../../../../../trainer/src/config"
import {getSessionUser} from "../../userSession";

/**
 * Graphql wrapper
 * @param query
 * @param params
 * @param method
 * @param useCache
 */
export default function (query, params = null, method = null, useCache = false, api) {
   const auth=getSessionUser()._id

    api|| (api=cfg.api)
    //pack params to query

    return fetcher(api, {query: query}, null, auth).then(
        result => {
            return {
                data: getCAdata(result),
                fullResult: result,
                error: false
            }
        }
    )
}


export  function getCAdata(data) {
    if (data.data && data.data.ca) data = data.data.ca

    for (let c in data) {
        for (let a in data[c]) {
            var o = data[c][a]
          //  o._fullResult = data
            return o
        }
    }
    ;

}
