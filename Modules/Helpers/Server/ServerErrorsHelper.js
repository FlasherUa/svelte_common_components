import { ERR_CODES } from "../../../../../../server/app/@types/responce";
/**
 *
 * @param result stop executing {true} or not
 */
export default (result) => {
    if (result.error.code === ERR_CODES.NOT_AUTH) {
        document.location.hash = "logout";
        return true;
    }
    return false;
};
