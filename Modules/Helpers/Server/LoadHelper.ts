let _fetcher:Function

export function init (fetch:Function) {
    _fetcher=fetcher
}

import fetcher from './fetcher'
import {iLoadComponent} from "../../../@types/core";

import {set} from "../../../_shared"

const _loads:any={}
set("Loads", _loads)

export default function (url:string,data:any=null,beforeLoad:Function=null,afterLoad:Function=null,onError:Function=null):Promise<any>{
    if (beforeLoad) beforeLoad()
    //@ts-ignore
    const promise = fetcher (url,data)
    //@ts-ignore
    if (afterLoad) promise.then(afterLoad)
    return promise
}


export function register (name:string, value:iLoadComponent) {
    _loads[name]=value
}

export function unregister (name:string) {
    delete _loads[name]
}

export function getLoad(name:string):iLoadComponent {
    if (_loads[name]) return  _loads[name]
    console.log(" no load for " +name)
    //fake mock to prevent error
    return {reload:()=>{}}
}