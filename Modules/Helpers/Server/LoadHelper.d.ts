export declare function init(fetch: Function): void;
import { iLoadComponent } from "../../../@types/core";
export default function (url: string, data?: any, beforeLoad?: Function, afterLoad?: Function, onError?: Function): Promise<any>;
export declare function register(name: string, value: iLoadComponent): void;
export declare function unregister(name: string): void;
export declare function getLoad(name: string): iLoadComponent;
