declare const _default: (result: any) => boolean;
/**
 *
 * @param result stop executing {true} or not
 */
export default _default;
