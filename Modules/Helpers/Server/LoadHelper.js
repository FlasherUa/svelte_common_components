let _fetcher;
export function init(fetch) {
    _fetcher = fetcher;
}
import fetcher from './fetcher';
import { set } from "../../../_shared";
const _loads = {};
set("Loads", _loads);
export default function (url, data = null, beforeLoad = null, afterLoad = null, onError = null) {
    if (beforeLoad)
        beforeLoad();
    //@ts-ignore
    const promise = fetcher(url, data);
    //@ts-ignore
    if (afterLoad)
        promise.then(afterLoad);
    return promise;
}
export function register(name, value) {
    _loads[name] = value;
}
export function unregister(name) {
    delete _loads[name];
}
export function getLoad(name) {
    if (_loads[name])
        return _loads[name];
    console.log(" no load for " + name);
    //fake mock to prevent error
    return { reload: () => { } };
}
