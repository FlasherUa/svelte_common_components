import objectToQueryString from "./objectToQueryString"
import ServerErrorsHelper from "../ServerErrorsHelper.ts";

export default function (api) {

    return function (url, params, method, auth = null) {

        method || (method = "GET")
        method = method.toUpperCase()
        let promiseResponce;

        if (method === "GET") {
            /**
             * DO GET
             */
            if (params) url += "?" + objectToQueryString(params)
            promiseResponce = fetch(api + url,
                {
                    method: "GET",
                    credentials: "include",
                    /*headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body:{}*/
                }
            )
        } else {
            /**
             * DO POST
             */
            promiseResponce = fetch(api + url,
                {
                    method: "POST",
                    credentials: "include",
                    headers: {
                        "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                    },
                    body: objectToQueryString(params)
                }
            )
        }

        var promise = promiseResponce.then(function (result) {
            //check for http errors
            if (!hasErr(result)) return result.json()

        }).then((result) => {
            //catch global domain errors
            if (result.error && ServerErrorsHelper(result)) throw "Server error " + result.error.value
            return {
                data: result,
                fullResult: result,
                //send local domain errors
                error: result.error ? result.error : false
            }
        })
        promise.catch(error => ({error}))

        return promise
    }
}

//check protocol errors
const hasErr = function (response) {
    if (response.status >= 200 && response.status < 300) {
        return false;
    } else {
        var error = new Error(response.statusText)
        error.response = response
        return error
        // throw error
    }
};
