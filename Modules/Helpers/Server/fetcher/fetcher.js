import objectToQueryString from "./objectToQueryString"


export default function (url, params, method, auth=null) {

    method || (method = "GET")
    method = method.toUpperCase()
    let promiseResponce;

    if (method === "GET") {
        /**
         * DO GET
         */
        if (params) url += "?" + objectToQueryString(params)
        promiseResponce = fetch(url,
            {
                method: method,
                credentials: "same-origin",
                headers: {
                    auth:auth
                }
                //  contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        )
    } else {
        /**
         * DO POST
         */
        var data = new FormData();
        data.append("json", JSON.stringify(params));
        promiseResponce = fetch(url,
            {
                method: method,
                // credentials: "same-origin",
                headers: {
                    "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
                body: params //String р
            }
        )
    }

    var promise = promiseResponce.then(function (result) {
        hasErr(result)
        return result.json()
    })

    return promise
}


const hasErr = function (response) {
    if (response.status >= 200 && response.status < 300) {
        return false;
    } else {
        var error = new Error(response.statusText)
        error.response = response
        throw error
    }
};
