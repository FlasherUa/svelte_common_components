import {ERR_CODES} from "../../../../../../server/app/@types/responce";
import cfg from "../../../../config"
/**
 *
 * @param result stop executing {true} or not
 */
export default (result:any):boolean=>{
     if (result.error.code===ERR_CODES.NOT_AUTH) {
         document.location.hash="logout"
         return true
     }
     return false
}
