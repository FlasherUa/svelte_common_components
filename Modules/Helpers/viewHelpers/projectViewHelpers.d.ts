/**
 * Convert goal_1 to string goal
 * @param goal_id
 * @return {string}
 * @deprecated use trainer/svelte3/review/projectRootForCommon/ComponentHelpers/config_helpers.ts
 */
export declare const getGoalString: (goal_id: number) => string;
interface iInfoOptions {
    noName: boolean;
}
/**
 * Return user info in string
 * if not clear html has bold tags
 * @param item
 * @param options
 */
export declare const user_info_string: (item: any, options?: iInfoOptions | undefined) => string;
/**
 * Conver sport id to sport string
 * @param item
 * @returns {string}
 */
export declare const sport: (item: any) => string;
export declare const getAllPodsValuesSelected: () => Object;
export declare const getPodColorByType: (type: any) => any;
/**
 * add local link to Store
 */
export declare const initStoreLink: ($store: any) => void;
/**
 * sets inner state collapseTableStyle value  styles based on collapsed value
 *
 * @context Svelte component
 * @param isCollapsed
 */
import { tTables } from "./dataUtils";
export declare const collapseTableHelper: (type: tTables) => void;
export declare function scrollEl(id: string): void;
/**
 * Tooltips helper
 */
export declare const tooltip: (key: string) => [string, string];
export declare function getOffset(el: HTMLElement): {
    top: number;
    left: number;
};
export declare const getStore: () => any;
export interface IUserStatus {
    color: string;
    title: string;
    id: number;
}
export declare const getUserStatus: (id: number) => IUserStatus;
export {};
