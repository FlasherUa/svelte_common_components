/**
 *
 * @param buttonEl on over button EL
 * @param popupEl popup itself EL
 * @param parentEl parent to attach EL (body)
 */
export declare function show(buttonEl: HTMLElement, popupEl: HTMLElement, parentEl?: HTMLElement): void;
export declare function hide(popupEl: HTMLElement): void;
/**
 * If dropdown not fits in visible area of browser window, flip it to top or to bottom
 */
declare function flipVerticalIfOutOfBounds(buttonEl: HTMLElement, popupEl: HTMLElement): void;
export { flipVerticalIfOutOfBounds };
