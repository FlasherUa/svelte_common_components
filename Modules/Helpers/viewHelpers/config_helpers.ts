export interface IUserStatus{
    color:string
    title:string
    id:number
}
//@ts-ignore
import {config} from "../../../../projectRootForCommon/plant/MainStore"
import {get} from "svelte/store"

export const getUserStatus =function (id:number):IUserStatus {
    const $config:any=get(config)
    const   statuses:Array<IUserStatus>=$config.app_user_statuses
    return statuses[id]
}

/**
 * Convert goal_1 to string goal
 * @param goal_id
 * @return {string}
 */
export const getGoalString = (goal_id: number): string => {
    const $config:any =get(config)
    return $config&& $config.app_goal_1 ? $config.app_goal_1[goal_id] : ""
}

interface iInfoOptions {
    noName:boolean
}

/**
 * Return user info in string
 * if not clear html has bold tags
 * @param item
 * @param options
 */
export const user_info_string = (item: any,options?:iInfoOptions) => {
    const
        he = item.gender === "male" ? "He" : "She",
        his = item.gender === "male" ? "His" : "Her",
        goal = getGoalString(item.goal_1),
        b_tag="<b>",// clearHTML ? "" :"<b>",
        b_tag_close = "</b>"//clearHTML ? "" :"</b>"

    let out = (options && options.noName ? "" : `${item.first_name}`) +
        ` is ${b_tag}${item.age}${b_tag_close},  ${b_tag}${item.weight}${b_tag_close} kg and `
        +`${b_tag}${item.height}${b_tag_close} cm. `;
    if (item.goal_1 && item.goal_1 != 0) out += `${he} wants to ${b_tag}${goal}${b_tag_close}.`
    if (item.rating) out += " " + his + ` fitto rank is ${b_tag} ${item.rating}${b_tag_close}`;

    return out
}
