import {IOption} from "../../../Components/Forms/formsInterfaces";

export  const arrToOptions=function (arr:string[]):IOption[] {
    const a:IOption[] =arr.map((item,index)=>({title:item, value:index}) )
    return a
}