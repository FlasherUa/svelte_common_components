export const arrToOptions = function (arr) {
    const a = arr.map((item, index) => ({ title: item, value: index }));
    return a;
};
