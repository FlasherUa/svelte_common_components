export type tTables = "manage" | "review" | "monitor"

const rowHeight = 57,
    heights = {manage: 450, review: 240, monitor: 240},
    buttonsHeight = 0,
    minRows =5;

const rowsHeight = (type: tTables): number =>  window.innerHeight - heights[type]

/**
 * Table rows count  (= first page size)
 * @param type
 */
export const getRowsCount = (type: tTables): number => {
 const rows  = Math.floor(rowsHeight(type) / rowHeight)
    return Math.max(minRows, rows)
}

/**
 * returns  table block height
 */

export const getHeight = (type: tTables) =>  rowsHeight(type)+(type=="manage" ? 100  : 0 )