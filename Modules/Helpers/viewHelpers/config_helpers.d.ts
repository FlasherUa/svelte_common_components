export interface IUserStatus {
    color: string;
    title: string;
    id: number;
}
export declare const getUserStatus: (id: number) => IUserStatus;
/**
 * Convert goal_1 to string goal
 * @param goal_id
 * @return {string}
 */
export declare const getGoalString: (goal_id: number) => string;
interface iInfoOptions {
    noName: boolean;
}
/**
 * Return user info in string
 * if not clear html has bold tags
 * @param item
 * @param options
 */
export declare const user_info_string: (item: any, options?: iInfoOptions | undefined) => string;
export {};
