//import store from "../../store"
import {debounce} from "../data/_";
//@DEPRECATED !! trainer/svelte3/review/src/ComponentHelpers/config_helpers.ts
let Store: any;
/**
 * Convert goal_1 to string goal
 * @param goal_id
 * @return {string}
 * @deprecated use trainer/svelte3/review/projectRootForCommon/ComponentHelpers/config_helpers.ts
 */
export const getGoalString = (goal_id: number): string => {
    const $data = Store.get().data

    return $data.appFilterCfg && $data.appFilterCfg.goal_1.value ? $data.appFilterCfg.goal_1.value[goal_id] : ""
}

interface iInfoOptions {
    noName:boolean
}

/**
 * Return user info in string
 * if not clear html has bold tags
 * @param item
 * @param options
 */
export const user_info_string = (item: any,options?:iInfoOptions) => {
    const
        he = item.gender === "male" ? "He" : "She",
        his = item.gender === "male" ? "His" : "Her",
        goal = getGoalString(item.goal_1),
        b_tag="<b>",// clearHTML ? "" :"<b>",
        b_tag_close = "</b>"//clearHTML ? "" :"</b>"

    let out = (options && options.noName ? "" : `${item.first_name}`) +
        ` is ${b_tag}${item.age}${b_tag_close},  ${b_tag}${item.weight}${b_tag_close} kg and `
        +`${b_tag}${item.height}${b_tag_close} cm. `;
    if (item.goal_1 && item.goal_1 != 0) out += `${he} wants to ${b_tag}${goal}${b_tag_close}.`
    if (item.rating) out += " " + his + ` fitto rank is ${b_tag} ${item.rating}${b_tag_close}`;

    return out
}

/**
 * Conver sport id to sport string
 * @param item
 * @returns {string}
 */
export const sport = (item: any): string => {
    let arr;
    try {
        arr = JSON.parse(item)
    } catch (e) {
        //is text
        return item
    }
    if (!Array.isArray(arr)) {
        arr = [arr]
    }
    const data = Store.get().data.appFilterCfg
    let out = [];
    for (let i = 0; i < arr.length; i++) {
        if (item && item != 0) out.push(data.top_sports.value[item])
    }
    return out.join(", ")
}

export const getAllPodsValuesSelected = (): Object => {
    return {1: true, 2: true, 3: true, 4: true}
}
export const getPodColorByType = function (type: any) {
    const $data = Store.get().data
    return $data['pod_types'] && $data.pod_types.items[type - 1] ? $data.pod_types.items[type - 1] : {}
}

/**
 * add local link to Store
 */
export const initStoreLink = ($store: any) => {
    Store = $store
}

/**
 * sets inner state collapseTableStyle value  styles based on collapsed value
 *
 * @context Svelte component
 * @param isCollapsed
 */
import {tTables,getHeight} from "./dataUtils";

export const collapseTableHelper = function (type:tTables) {

    const state = this.get()
    let _collapseTableStyle = "", _collapseTbodyStyle = ""

    if (!state.isCollapsed) {

        const margin_bottom = 20,
            table_margins = 20,
            headerFix=10,

            table = this.refs.tableRef,
            //document.documentElement.clientHeight - table.offsetTop - margin_bottom - table_margins,
            theader = table.querySelector ("thead"),
            theader_height:number= theader ? theader.offsetHeight-headerFix : 0,
            trArr:NodeList= table.querySelectorAll (".table-bordered > tbody > tr"),
            trsHeights:number= getHeight(type), //Array.from(trArr).reduce ((sum:number=0, tr:HTMLElement,i:number)=>i<12? sum+tr.offsetHeight : sum , 0),
            table_height = trsHeights+headerFix,
            tbody_height= table_height-theader_height

        _collapseTableStyle = "height:" + table_height + "px; max-height:"+ table_height + "px;"
        _collapseTbodyStyle = "height:" + tbody_height + "px; max-height:"+ tbody_height + "px;"
    }
    if (state._collapseTableStyle !== _collapseTableStyle) this.set({_collapseTableStyle, _collapseTbodyStyle})
}

export function scrollEl (id:string){
    const el:any=document.getElementById(id)
    if (el) {
        let foo
        if (el.scrollIntoViewIfNeeded) foo = el.scrollIntoViewIfNeeded
        else foo = el.scrollIntoView
        const de=debounce(foo, 500)

        de.call(el)
    }
}

/**
 * Tooltips helper
 */
export const tooltip= function (key:string):[string,string] {
    const tooltips = Store.get()._tooltips,
        obj=tooltips["tooltips control "+key]

    return ["ss","ss"]
}

export function getOffset( el:HTMLElement ):{top:number,left:number} {
    const rect = el.getBoundingClientRect();
    return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY
    };
}


export const getStore = () => Store

export interface IUserStatus{
    color:string
    title:string
    id:number
}

//import DataKeeper from "../../componentsJocker/core/DataKeeper";

export const getUserStatus =function (id:number):IUserStatus {
    //@ts-ignore
    const dk= DataKeeper("Data_listAppConfig"),
        statuses:Array<IUserStatus>=dk.get().loaded_data.appFilterCfg.user_statuses.value
    return statuses[id]
}