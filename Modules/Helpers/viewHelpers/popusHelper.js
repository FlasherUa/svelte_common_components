/**
 *
 * @param buttonEl on over button EL
 * @param popupEl popup itself EL
 * @param parentEl parent to attach EL (body)
 */
export function show(buttonEl, popupEl, parentEl = null) {
    parentEl || (parentEl = document.body);
    //save position
    popupEl.style.display = "block";
    const posButton = buttonEl.getBoundingClientRect(), top = posButton.top, left = posButton.left, posPopup = popupEl.getBoundingClientRect(), popupWidth = parseInt(popupEl.style.width), //need to be in pixels
    popupHeight = posPopup.height, bodyWidth = document.body.clientWidth, bodyHeight = document.body.clientHeight, 
    //shift logic
    needMoveLeft = bodyWidth - left - popupWidth;
    //attach to parent
    parentEl.appendChild(popupEl);
    popupEl.style.display = "inherit";
    //preserver styles
    //  popupEl.style.cssText=css
    if (needMoveLeft < 0)
        popupEl.style.left = String(left + needMoveLeft - 20);
    else
        popupEl.style.left = left;
    popupEl.style.top = top + 10;
    /*if ((top + popupHeight.height) > bodyHeight) {

       // this.set({dropdownStyle: `bottom: ${popupHeight.height}px;`});
        popupEl.style.bottom = top
    } else {
        / * default behaviour is from top to bottom, clear styles * /
        //this.set({dropdownStyle: ''});
        popupEl.style.top = top
    }*/
}
export function hide(popupEl) {
    popupEl.style.display = "none";
}
/**
 * If dropdown not fits in visible area of browser window, flip it to top or to bottom
 */
function flipVerticalIfOutOfBounds(buttonEl, popupEl) {
    let label = buttonEl.getBoundingClientRect(), bodyHeight = document.body.getBoundingClientRect().height, dropdownRect = popupEl.getBoundingClientRect();
}
export { flipVerticalIfOutOfBounds };
