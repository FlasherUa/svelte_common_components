import { getStore } from "../projectViewHelpers";
/**
 * adds user data to global user selecttion
 * @param data
 */
export const userSelect = function (data) {
    //getStore().set ({_user_selected: data})
    const store = getStore(), $data = store.get().data, id = data.id;
    //set user for tables filter
    $data.DATA_users_by_filter._search = data.first_name;
    $data.DATA_users_by_filter._searchId = data.id; //data.first_name
    //set user for review graphs
    store.set({
        data: $data,
        review_selected_user_id: id,
        review_selected_user_data: data
    });
};
export const userClear = function () {
    const store = getStore(), $data = store.get().data;
    $data.DATA_users_by_filter._search = "";
    $data.DATA_users_by_filter._searchId = 0;
    store.set({
        data: $data,
        review_selected_user_id: undefined,
        review_selected_user_data: {}
    });
};
/**
 * Component method
 *
 * @param page
 * @param userData
 */
export const routeToPageUserWithSelected = function (page, userData) {
    userSelect(userData);
    this.root.getRoute(page);
};
