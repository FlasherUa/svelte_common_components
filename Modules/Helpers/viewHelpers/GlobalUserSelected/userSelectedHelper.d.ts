/**
 * adds user data to global user selecttion
 * @param data
 */
export declare const userSelect: (data: any) => void;
export declare const userClear: () => void;
/**
 * Component method
 *
 * @param page
 * @param userData
 */
export declare const routeToPageUserWithSelected: (page: String, userData: any) => void;
