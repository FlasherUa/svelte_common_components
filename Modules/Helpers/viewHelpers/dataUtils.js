const rowHeight = 57, heights = { manage: 450, review: 240, monitor: 240 }, buttonsHeight = 0, minRows = 5;
const rowsHeight = (type) => window.innerHeight - heights[type];
/**
 * Table rows count  (= first page size)
 * @param type
 */
export const getRowsCount = (type) => {
    const rows = Math.floor(rowsHeight(type) / rowHeight);
    return Math.max(minRows, rows);
};
/**
 * returns  table block height
 */
export const getHeight = (type) => rowsHeight(type) + (type == "manage" ? 100 : 0);
