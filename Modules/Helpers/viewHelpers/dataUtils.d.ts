export declare type tTables = "manage" | "review" | "monitor";
/**
 * Table rows count  (= first page size)
 * @param type
 */
export declare const getRowsCount: (type: tTables) => number;
/**
 * returns  table block height
 */
export declare const getHeight: (type: tTables) => number;
