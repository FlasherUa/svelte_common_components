//@ts-ignore
import routes_tree from "../../../Dashboard/plant/routes_tree"
import {go} from "../../router"

export const  findFirstRoute =function   (menuName:string):string {
   return  routes_tree[menuName][0].route
}

export const navigateFirstRoute =function(menuName:string):void{
    const route=findFirstRoute(menuName)
    go(route)
}

