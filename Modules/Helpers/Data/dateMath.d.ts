export declare const roundDay: (unix_timestamp: number) => number;
export declare const getHours: (unix_timestamp: number) => number;
export declare const weekdaysDropDown: ({
    value: string;
    title: string;
    short: string;
} | {
    value: number;
    title: string;
    short: string;
})[];
declare const _default: {
    roundDay: (unix_timestamp: number) => number;
    getHours: (unix_timestamp: number) => number;
    weekdaysDropDown: ({
        value: string;
        title: string;
        short: string;
    } | {
        value: number;
        title: string;
        short: string;
    })[];
};
export default _default;
