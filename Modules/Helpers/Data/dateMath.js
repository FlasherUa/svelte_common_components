export const roundDay = function (unix_timestamp) {
    var currentDate = new Date(unix_timestamp * 1000);
    var date = currentDate.getDate();
    var month = currentDate.getMonth(); //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();
    var dateString = year + "-" + (month + 1) + "-" + date;
    var d2 = new Date(dateString);
    return d2.getTime() / 1000;
};
export const getHours = function (unix_timestamp) {
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours() + date.getMinutes() / 100 * 1.6;
    return hours;
};
export const weekdaysDropDown = [
    { value: "0", title: "Sunday", short: "Sun" },
    { value: 1, title: "Monday", short: "Mon" },
    { value: 2, title: "Tuesday", short: "Tue" },
    { value: 3, title: "Wednesday", short: "Wed" },
    { value: 4, title: "Thursday", short: "Thu" },
    { value: 5, title: "Friday", short: "Fri" },
    { value: 6, title: "Saturday", short: "Sat" }
];
export default {
    roundDay,
    getHours,
    weekdaysDropDown
};
