export declare var debounce: (func: Function, wait: number, immediate?: boolean) => Function;
export declare const addslashes: (str: string) => string;
/***
 * Safe consvert
 * @param
 */
export declare const safe_JSON_parse: (s: string) => any;
