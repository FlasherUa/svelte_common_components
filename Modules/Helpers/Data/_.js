export var debounce = function (func, wait, immediate = false) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        if (arguments[0] == "CLEAR") {
            clearTimeout(timeout);
            return;
        }
        var later = function () {
            timeout = null;
            if (!immediate)
                func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow)
            func.apply(context, args);
    };
};
export const addslashes = (str) => {
    str = JSON.stringify(String(str));
    str = str.substring(1, str.length - 1);
    return str;
};
/***
 * Safe consvert
 * @param
 */
export const safe_JSON_parse = function (s) {
    let o = {};
    try {
        o = JSON.parse(s);
    }
    catch (e) {
        return false;
    }
    return o;
};
