/**
 *
 * @param {undefined} unix timestamp
 */
export default function (unix, format) {

    var a = new Date(unix * 1000);
    return formatDate(a, format)
        ;

}

/**
 *
 * @param a Date object
 * @return {*}
 */
export const formatDate = function (a, format) {
    typeof format !== "undefined" || (format = 0)

    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        daysWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        daysWeekShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        yearFull = a.getFullYear(),
        year = yearFull - 2000,
        monthNum = a.getMonth()+1,
        monthNumZero = a.getMonth(),
        month = months[monthNum-1],
        date = a.getDate(),
        hour = a.getHours(),
        min = a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes(),
        sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds(),
        time
    if (format === 1) time = '' + date + '-' + month + ' ' + hour + ':' + min; //no year
    else if (format === 2) time = '' + yearFull + '-' + startZero(monthNum ) + '-' + startZero(date) + 'T' + startZero(hour) + ':' + startZero(min); // "2019-01-22T04:45 format for date input form
    else if (format === 3) time = date + "<br>" + month; // "2019-01-22T04:45 format for date input form
    else if (format === 3.1) time = date + " " + month; // "2019-01-22T04:45 format for date input form
    else if (format === "d/m/y") time = startZero(date)+"/"+ startZero(monthNum)+"/"+yearFull; // "4/5/2019 format for daterange input form
    else if (format === 4) time = hour + ':' + min; // "2019-01-22T04:45 format for date input form
    else if (format === 5) time = daysWeek[a.getDay()]; // Sunday
    else if (format === 6) time = '' + date + ', ' + month
    else if (format === 'dayWeekShort') time = daysWeekShort[a.getDay()]; // Sunday
    else time = '' + date + '-' + month + '-' + year + ' ' + hour + ':' + min; //21-12-2019 14:55
    return time;
}


const startZero = function (num) {
    if (num < 10) return "0" + parseInt(num)
    return num
}


export const roundDay = function (unix_timestamp) {
    var currentDate = new Date(unix_timestamp * 1000)

    var date = currentDate.getDate();
    var month = currentDate.getMonth(); //Be careful! January is 0 not 1
    var year = currentDate.getFullYear();
    var dateString = year + "-" +startZero (month + 1) + "-" +startZero(date);
    var d2 = new Date(dateString)
    return d2.getTime() / 1000
}

export const getHours = function (unix_timestamp) {
    var date = new Date(unix_timestamp * 1000);
    // Hours part from the timestamp
    var hours = date.getHours() + date.getMinutes() / 100 * 1.6;
    return hours

}
