import { TrouteParams } from "../../../../../../webApps2/src/Common/@types/core";
export declare function hashToObject(url: string): TrouteParams;
export declare function objectToHash(route: string, obj?: any | string): string;
