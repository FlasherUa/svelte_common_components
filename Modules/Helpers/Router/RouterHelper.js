export function hashToObject(url) {
    const result = {}, res = { route: "", params: result }, firstSlash = url.indexOf("/");
    if (firstSlash === -1) {
        res.route = url;
    }
    else {
        res.route = url.substr(0, firstSlash);
        const paramsToSplit = url.substr(firstSlash + 1), pairs = paramsToSplit.split('/');
        if (pairs.length > 1) {
            pairs.forEach((x, i, a) => {
                if (i % 2) {
                    //comma separated list to array
                    const hasCommas = x.indexOf(",") > -1, val = hasCommas ? x.split(",") : x;
                    result[a[i - 1]] = val;
                }
            });
        }
        else {
            res.params = paramsToSplit;
        }
    }
    return res;
}
export function objectToHash(route, obj) {
    if (!obj)
        return route;
    let out = route;
    if (typeof obj === 'string') {
        //is scalar
        out = "/" + obj;
    }
    else {
        //is object
        for (let k in obj) {
            const param = obj[k];
            out += "/" + k + "/";
            out += Array.isArray(param) ? param.join(",") : param;
        }
    }
    return out;
}
