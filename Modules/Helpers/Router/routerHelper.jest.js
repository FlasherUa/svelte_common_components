import { hashToObject } from "./RouterHelper";
const cases = {
    "manage": {
        route: "manage",
        //@ts-ignore
        params: {}
    },
    "manage/": {
        route: "manage",
        //@ts-ignore
        params: {}
    },
    "manage/45": {
        route: "manage",
        params: 45
    },
    "manage/users/12": {
        route: "manage",
        params: { users: "12" }
    },
    "manage/users/12,34,56": {
        route: "manage",
        params: { users: ["12", "34", "56"] }
    }
};
export default function testRouters() {
    debugger;
    //@ts-ignore
    for (let key in cases) {
        //@ts-ignore
        const cse = cases[key], result = hashToObject(key), test = result.toString() === cse.toString(), out = test ? key + " Ok" : "Error " + result.toString() + " !== " + cse.toString();
        if (!test)
            throw out;
        else
            console.log(out);
    }
}
testRouters();
