export  const logout = function () {

   delete window.localStorage.userState //JSON.stringify(data)
  // window.localStorage.userName = data.name

}

export  const login = function (data:any) {
        console.log(data)
    window.localStorage.userState = JSON.stringify(data)
    window.localStorage.userName = data.name

}

interface IuserData {
    _id:string
    id:string
    name:string
}

let _userData:IuserData

export  const getSessionUser = function ():IuserData|null {
    if (_userData) return _userData

    const user=window.localStorage.userState
        let userData
        try {
            userData= JSON.parse(user)
        }catch(e){
            userData=  null
        }
    _userData=userData
    return  userData
}