import { config } from "../../../../projectRootForCommon/plant/MainStore";
import { get } from "svelte/store";
import CONST from "../../../Project/CONST";
import { arrToOptions } from "../viewHelpers/optionsHelper";
export const filterConfig = function (name) {
    let items = [];
    if (name === 'ages' || name === 'gender') {
        items = arrToOptions(CONST[name]);
        return items;
    }
    if (name === 'orderBy') {
        items = [{ title: 'GOAL', value: 'goal_1' }, { value: 'age', title: 'AGE' }, {
                value: "gender",
                title: "GENDER"
            }];
        return items;
    }
    const $config = get(config), cfg = $config[name];
    if (!cfg)
        return items;
    const val = cfg.value;
    items = arrToOptions(val);
    //items = //$config.appFilterCfg[name].value
    return items;
};
