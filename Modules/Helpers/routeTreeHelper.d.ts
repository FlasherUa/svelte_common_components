export declare const findFirstRoute: (menuName: string) => string;
export declare const navigateFirstRoute: (menuName: string) => void;
