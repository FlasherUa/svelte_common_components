export const logout = function () {
    delete window.localStorage.userState; //JSON.stringify(data)
    // window.localStorage.userName = data.name
};
export const login = function (data) {
    console.log(data);
    window.localStorage.userState = JSON.stringify(data);
    window.localStorage.userName = data.name;
};
let _userData;
export const getSessionUser = function () {
    if (_userData)
        return _userData;
    const user = window.localStorage.userState;
    let userData;
    try {
        userData = JSON.parse(user);
    }
    catch (e) {
        userData = null;
    }
    _userData = userData;
    return userData;
};
