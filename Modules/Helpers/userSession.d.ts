export declare const logout: () => void;
export declare const login: (data: any) => void;
interface IuserData {
    _id: string;
    id: string;
    name: string;
}
export declare const getSessionUser: () => IuserData | null;
export {};
