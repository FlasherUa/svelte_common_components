export default class EventsClass {
    constructor() {
        this.events = {};
    }
    on(eventName, fn) {
        this.events[eventName] = this.events[eventName] || [];
        this.events[eventName].push(fn);
    }
    off(eventName, fn) {
        if (this.events[eventName]) {
            for (var i = 0; i < this.events[eventName].length; i++) {
                if (this.events[eventName][i] === fn) {
                    this.events[eventName].splice(i, 1);
                    break;
                }
            }
            ;
        }
    }
    trigger(eventName, data = null) {
        if (this.events[eventName]) {
            var that = this;
            this.events[eventName].forEach(function (fn) {
                fn.call(that, data);
            });
        }
    }
    subscribeToList(eventsList) {
        for (let eventName in eventsList) {
            this.on(eventName, eventsList[eventName]);
        }
    }
}
;
