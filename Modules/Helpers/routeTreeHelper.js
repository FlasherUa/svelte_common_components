//@ts-ignore
import routes_tree from "../../../Dashboard/plant/routes_tree";
import { go } from "../../router";
export const findFirstRoute = function (menuName) {
    return routes_tree[menuName][0].route;
};
export const navigateFirstRoute = function (menuName) {
    const route = findFirstRoute(menuName);
    go(route);
};
