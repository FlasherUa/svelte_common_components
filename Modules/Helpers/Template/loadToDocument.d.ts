/**
 *
 * @param path
 * @param callback if no, returns promise
 */
export declare function loadJs(path: string, callback?: Function): Promise<any>;
export declare function loadCss(path: string, callback?: Function): void;
