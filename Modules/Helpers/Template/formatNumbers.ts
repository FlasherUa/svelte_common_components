export const thousands = (x:number|string):string => {
    let parts:Array<string> = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
