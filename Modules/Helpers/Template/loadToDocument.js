const _loaded = {};
/**
 *
 * @param path
 * @param callback if no, returns promise
 */
export function loadJs(path, callback) {
    if (_loaded[path]) {
        if (callback)
            callback();
        return;
    }
    _loaded[path] = true;
    let script = document.createElement('script'), promise;
    script.type = "text/javascript";
    //@ts-ignore
    if (callback)
        script.onload = callback;
    else {
        promise = new Promise((resolve, reject) => script.onload = resolve);
    }
    script.src = path;
    document.head.appendChild(script);
    return promise;
}
export function loadCss(path, callback) {
    if (_loaded[path]) {
        return;
    }
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = path;
    link.media = 'all';
    head.appendChild(link);
}
