export default class EventsClass {
    events: any;
    on(eventName: string, fn: Function): void;
    off(eventName: string, fn: Function): void;
    trigger(eventName: string, data?: any): void;
    subscribeToList(eventsList: any): void;
}
