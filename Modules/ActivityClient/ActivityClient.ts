/// <reference path="../../../../../server/app/object_action/Activity/Activity.d.ts" />
//@ts-ignore
import cfg from "../../../../../server/app/object_action/Activity/cfg/client";
import { go } from "../../router";
import ActivityStepFactory from "../../../../../server/app/object_action/Activity/Activity"
import config from "../../../config"



const runStepServer=function (activity_name:string, stepCfg:TActivityStep,state:TActivityStepState){
//call server
        const data:IActivityStepClientToServerMessage={
            activity:activity_name,
            state,
            step:stepCfg.no
        }
        config.fetcher("activity" ,data )
},
    runStepClient=function (activity_name:string, stepCfg:TActivityStep,state:TActivityStepState){
    if (stepCfg.action !== "route") return
    const stateNew = { ...stepCfg.state.route_data, ...state}
    runRoute(stepCfg.state.route,stateNew)
}

const runRoute = function (route: string, state: any) {
    go(route, state)
}

export  default ActivityStepFactory (cfg,runStepServer,runStepClient )
