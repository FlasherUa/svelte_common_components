import { TrouteParams } from "./@types/core";
/**
 * Checks if current route
 * @param pattern
 * @param routeObj
 * @param curStep
 * @returns {boolean}
 */
export declare const testRoute: (pattern: string) => boolean;
export declare const getCurrentRoute: () => {
    route: string;
    routeParams: TrouteParams;
};
export declare const isActive: (link: string, $route0: string) => boolean;
export declare function go(route: string, params?: any, silent?: boolean): void;
export declare function start(route0?: any, defaultRoute0?: any, routeParams0?: any): void;
export declare function stop(): void;
