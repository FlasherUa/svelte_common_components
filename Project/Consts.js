import CONST from "./CONST.js";
export const filterConfig = function (name, data) {
    let items;
    if (name === 'ages' || name === 'gender') {
        items = arrayToValues(CONST[name]);
    }
    else if (name === 'orderBy') {
        items = [
            { value: "rating", title: "Rank" },
            { value: 'goal_1', title: 'Goal' },
            { value: 'age', title: 'Age' },
            { value: "gender", title: "Gender" },
        ];
    }
    else {
        items = arrayToValues(data[name].value);
    }
    return items;
};
export function arrayToValues(arr) {
    let out = [];
    for (let i = 0; i < arr.length; i++) {
        out.push({ title: arr[i],
            value: i.toString() });
    }
    return out;
}
