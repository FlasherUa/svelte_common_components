 
var CONST ={
    periods: ["Days", "Weeks", "Months", "Quarters", "Years"],
    period1: ["Month", "Quarter", "Year"],
    period2: ["Month", "Quarter"],
    monthDays: ["Days", "Months"],
    ages: ["All ages", "18-25", "25-35", "35-60", "Over 60"],
    gain: ["All goals", "Gain muscle mass", "Get trim & fit", "Lose weight", "Other"],
    sports: ["All sports", "Football", "Basketball", "Baseball", "Running", "Soccer", "Gym", "CrossFit", "Bicycles", "Yoga", "Other"],
    //pod_type_id - color
    podColors: ["", "180, 123, 255", "57, 139, 255", "57, 255, 122", "255, 173, 57"],
    gender:["All genders", "Male", "Female"],
    users: {
        "ee": {

        }, "e2": {

        }

    }
};export default CONST;/**
* get the CONST age index by age
* @param {undefined} age 
*/
 export  
var ageToIndex =  function (age) {  
//ages: ["All ages", "18-25", "25-35", "35-60", "Over 60"],
if (age < 25) return 1
if (age < 35) return 2
if (age < 60) return 3
return 4
; 

}
/**
* get the CONST age index by age
* @param {undefined} age 
*/
 export  
var genderToIndex =  function (age) {  
 //gender:["All genders", "Male", "Female"],

   
    if (age =="Male" || age=="male" ) return 1
    if (age =="Female" || age=="female"  ) return 2
    return ""
; 

}
