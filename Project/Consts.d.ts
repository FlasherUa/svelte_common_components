import { IOption } from "../Components/Forms/formsInterfaces";
export declare const filterConfig: (name: string, data: any) => IOption[];
export declare function arrayToValues(arr: Array<string>): Array<IOption>;
