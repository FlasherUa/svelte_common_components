//@ts-ignore
import plannedEventsHelper from "./plannedEventsHelper_v2"
//@ts-ignore

import getShared, {fetcher} from "../../../../_shared"
import {roundDay} from "../../../../Modules/Helpers/Data/dateMath";
//import DataKeeper from "../../../core/DataKeeper";
//import {userClear} from "../../../../utlis/viewHelpers/GlobalUserSelected/userSelectedHelper.js" 

//@ts-ignore
import getShared from "../../../../_shared"//../projectRootForCommon/plant/MainStore"
import {get} from "svelte/store"
import {config} from "../../../../CommonStore"

import {iOption} from "../../../../../../../webApps2/src/Common/@types/form_types";
/**
 * Computed
 *
 * @param $data
 * @param data
 */
export const pod_fav_items =function ():Array<any>{
    const cfg=get(config), ///get(config),
        pod_types=cfg.pod_types,
        _plan_favorites=cfg.plan_favorites,
        _pod_types_byId=cfg.pod_types_byId

    const i:Array<iOption>  = pod_types.filter(item=>item.isSuspended!=="yes")
        .map((item:any) => {
        return {
            title: "<span  class='pod-color-selector-item' style='background-color:#" + item.pod_color + "'></span>" + item.name,
            value: item.pod_type_id
        }
    })
    if (_plan_favorites ) {
             _plan_favorites.forEach ((item:any)=>{
             const pod= _pod_types_byId[item.pod_id]
            if(!pod) return
            i.push({
                title: "<i  class='favorite-plan-icon fa fa-star' style='color:#" + pod.pod_color + "'></i>" + item.name,
                value:{type:"fav", id: item.pod_ids, data: item }
            })
        })
    }
    return i
}

/**
 * Methods
 */

export const onClearAllClicked =  function () {
    const params = this.get().params
    /* params._checkedCount=0
     params.checked_arr=[]
     params.checked_users_data={}
     this.set(params)*/

   /* userClear() */
    this.store.set( {_clearManageChecked:true})
    this.root.fire("manage:clear")
    params.clearAll && params.clearAll()

}

/**
 * call server API function on selected users
 */
export const updatePlan = function (usersArr:Array<string>) {
    const users = usersArr.join(","),
        query = `{ca { pod_events{notifyOnPlanUpdated( users:"${users}"  ) {  _id }}}}`,
        promise = fetcher(query)

        promise.then (refreshData)
    //plannedEventsHelper.setEventsActive.call(this, usersArr);
    //this.store.set({isUpdatePlanAllowed: false})
}

/**
 * adds event to selected user(s)
 */
export const addEvent = function (state: any) {

    const expDate = state.expDate ? state.expDate : 0
    let
        users: number[] = state.user_selected2 ? [state.params._user_selected2] : Object.keys(state.selectedUsers)
    if (users.length==0) return
    let    repeatType = state.repeatType,

        repeatPeriod = repeatType === "no" ? 0 :
            repeatType === "daily" ? 1 :
                repeatType === "dailyNoW" ? 1 :
                    7,

        /* start date */
        expected = Math.floor(state.date),//new Date(state.date).getTime() / 1000,

        aWeek = 3600 * 24 * repeatPeriod, //hours in a week
        pod_type_id = state.pod_type_id,
        promises = []

    let repeat = repeatType != "no" ? state.repeat : 0

    /* Overwrite values for fav plan logic */

    if (state.isFavPlanSelected) {
        const fav_plan = state.pod_type_id.data;
        aWeek = 7 * 3600 * 24;
        pod_type_id = fav_plan.pod_id;
        repeat = fav_plan.times_repeat - 1;
        expected = roundDay(expDate) + parseInt(fav_plan.time) //start date + fav plan time
    }

    //this.store.set({"isUpdatePlanAllowed": true})

    const users_list = users.join(","),
        offset = new Date().getTimezoneOffset() * 60;


    for (let r = 0; r <= repeat; r++) {
        const date = expected + aWeek * r,//+ offset,
            dateJS = new Date(date * 1000),
            weekday = dateJS.getDay()
        if (repeatType === "dailyNoW" && (weekday === 6 || weekday === 0)) {
            repeat++;
            continue
        }
        const _id = Math.random(),
            query = `{ca { pod_events{addPlanned( _id: "${_id}" users_list:"${users_list}" pod_id:${pod_type_id} expected_at:${date} ) {  pod_event_id  expected_at }}}}`,
            eventData: any = {},
            // closure  intial event data
            promise = (function (eventData, query) {
                return fetcher(query)
            })(eventData, query);
        promises.push(promise)
    }
    Promise.all(promises).then(res => {
        /*refresh*/
        refreshData()
    })
}

export function refreshData () {
    getShared('Loads')['Load_manage_users_events'].reload()
}

export const addFavPlanToEvents=function () {


}
