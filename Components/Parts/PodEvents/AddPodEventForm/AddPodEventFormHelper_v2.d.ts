/**
 * Computed
 *
 * @param $data
 * @param data
 */
export declare const pod_fav_items: () => any[];
/**
 * Methods
 */
export declare const onClearAllClicked: () => void;
/**
 * call server API function on selected users
 */
export declare const updatePlan: (usersArr: string[]) => void;
/**
 * adds event to selected user(s)
 */
export declare const addEvent: (state: any) => void;
export declare function refreshData(): void;
export declare const addFavPlanToEvents: () => void;
