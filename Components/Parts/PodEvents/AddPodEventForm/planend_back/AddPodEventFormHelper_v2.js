//@ts-ignore
import runQuery from "../../../ComponentHelpers/runQuery";
import { roundDay } from "../../../../../../../webApps2/src/Common/Modules/Helpers/Data/dateMath";
//import DataKeeper from "../../../core/DataKeeper";
//import {userClear} from "../../../../utlis/viewHelpers/GlobalUserSelected/userSelectedHelper.js" 
//@ts-ignore
import { pod_types, plan_favorites, pod_types_byId } from "../../../plant/MainStore";
import { get } from "svelte/store";
/**
 * Computed
 *
 * @param $data
 * @param data
 */
export const pod_fav_items = function () {
    const i = get(pod_types).map((item) => {
        return {
            title: "<span  class='pod-color-selector-item' style='background-color:#" + item.pod_color + "'></span>" + item.name,
            value: item.pod_type_id
        };
    });
    const _plan_favorites = get(plan_favorites);
    if (_plan_favorites) {
        const _pod_types_byId = get(pod_types_byId);
        _plan_favorites.forEach((item) => {
            const pod = _pod_types_byId[item.pod_id];
            if (!pod)
                return;
            i.push({
                title: "<i  class='favorite-plan-icon fa fa-star' style='color:#" + pod.pod_color + "'></i>" + item.name,
                value: { type: "fav", id: item.pod_ids, data: item }
            });
        });
    }
    return i;
};
/**
 * Methods
 */
export const onClearAllClicked = function () {
    const params = this.get().params;
    /* params._checkedCount=0
     params.checked_arr=[]
     params.checked_users_data={}
     this.set(params)*/
    /* userClear() */
    this.store.set({ _clearManageChecked: true });
    this.root.fire("manage:clear");
    params.clearAll && params.clearAll();
};
/**
 * call server API function on selected users
 */
export const updatePlan = function () {
    const state = this.get(), usersArr = state.params.checked_arr, users = usersArr.join(","), query = `{ca { pod_events{notifyOnPlanUpdated( users:"${users}"  ) {  _id }}}}`, promise = runQuery(query);
    promise.then(refreshData);
    //plannedEventsHelper.setEventsActive.call(this, usersArr);
    //this.store.set({isUpdatePlanAllowed: false})
};
/**
 * adds event to selected user(s)
 */
export const addEvent = function (expDate = 0) {
    let state = this.get(), users = state.params._user_selected2 ? [state.params._user_selected2] : state.params.checked_arr, repeatType = state.repeatType, repeatPeriod = repeatType === "no" ? 0 :
        repeatType === "daily" ? 1 :
            repeatType === "dailyNoW" ? 1 :
                7, 
    /* start date */
    expected = Math.floor(state.date), //new Date(state.date).getTime() / 1000,
    aWeek = 3600 * 24 * repeatPeriod, //hours in a week
    pod_type_id = state.pod_type_id, promises = [], that = this;
    let repeat = repeatType != "no" ? state.repeat : 0;
    /* Overwrite values for fav plan logic */
    if (state.isFavPlanSelected) {
        const fav_plan = state.pod_type_id.data;
        aWeek = 7 * 3600 * 24;
        pod_type_id = fav_plan.pod_id;
        repeat = fav_plan.times_repeat - 1;
        expected = roundDay(expDate) + parseInt(fav_plan.time); //start date + fav plan time
    }
    this.store.set({ "isUpdatePlanAllowed": true });
    const users_list = users.join(","), offset = new Date().getTimezoneOffset() * 60;
    /* for (let u = 0; u < users.length; u++) {
     }*/
    for (let r = 0; r <= repeat; r++) {
        const date = expected + aWeek * r, //+ offset,
        dateJS = new Date(date * 1000), weekday = dateJS.getDay();
        if (repeatType === "dailyNoW" && (weekday === 6 || weekday === 0)) {
            repeat++;
            continue;
        }
        const _id = Math.random(), query = `{ca { pod_events{addPlanned( _id: "${_id}" users_list:"${users_list}" pod_id:${pod_type_id} expected_at:${date} ) {  pod_event_id  expected_at }}}}`, eventData = {}, //{user_id: users[u], pod_id: pod_type_id, expected_at: date, is_active: 0, pod_event_id:null},
        // closure  intial event data
        promise = (function (eventData, query) {
            return runQuery(query); /*.then(function (result) {
                   eventData.pod_event_id = result.data.ca.pod_events.addPlanned.pod_event_id
                   eventData.expected_at = result.data.ca.pod_events.addPlanned.expected_at
                   //add event to local store - just do refresh after all
                 //  plannedEventsHelper.addEventToLocalList.call(that, eventData);
               })*/
        })(eventData, query);
        promises.push(promise);
    }
    Promise.all(promises).then(res => {
        /*refresh*/
        refreshData();
    });
};
export function refreshData() {
    // DataKeeper('DATA_User_Planned_Events').refresh()
}
export const addFavPlanToEvents = function () {
};
