/**
 * Computed
 *
 * @param $data
 * @param data
 */
export declare const pod_fav_items: () => any[];
/**
 * Methods
 */
export declare const onClearAllClicked: () => void;
/**
 * call server API function on selected users
 */
export declare const updatePlan: () => void;
/**
 * adds event to selected user(s)
 */
export declare const addEvent: (expDate?: number) => void;
export declare function refreshData(): void;
export declare const addFavPlanToEvents: () => void;
