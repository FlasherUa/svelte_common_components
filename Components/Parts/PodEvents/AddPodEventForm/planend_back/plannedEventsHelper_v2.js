import {getStore} from "../../../../utlis/viewHelpers/projectViewHelpers"
import runQuery from "../../../../utlis/data/runQuery"
import {refreshData} from "./AddPodEventFormHelper";

export default {


    remove: function (id, ids) {
        removeOnServer(id, ids).then ( refreshData )
/*
        const store = getStore()
        const $planned_user_events = store.get().planned_user_events
        //search event
        for (let user in $planned_user_events) {
            for (let i = 0; i < $planned_user_events[user].items.length; i++) {
                const evt = $planned_user_events[user].items[i]
                if (evt.pod_event_id == id) {
                    $planned_user_events[user].items.splice(i, 1)
                    store.set({planned_user_events: $planned_user_events})
                    store.set({isUpdatePlanAllowed: true})
                    return
                }
            }
        }*/
    },
    addEventToLocalList: function (event) {
        const store = getStore()
        let $planned_user_events = store.get().planned_user_events,
            user_id = event.user_id
            if (typeof $planned_user_events=="undefined") $planned_user_events={}

        $planned_user_events[user_id] || ($planned_user_events[user_id] = {items: []})

        insertEvent($planned_user_events[user_id].items, event)
        store.set({planned_user_events: $planned_user_events})
    },
    setEventsActive: function (users) {
        if (!users || !users.length > 0) return

        const store = getStore()
        const $planned_user_events = store.get().planned_user_events
        if (!$planned_user_events) return
        users.forEach(function(user) {

            if (!$planned_user_events[users]) return
                const events = $planned_user_events[users].items
                if (!Array.isArray(events)) return
                events.forEach (function(event){
                    event.is_active=1
                })
            }
        )
        //insertEvent($planned_user_events[user_id].items, event)
        store.set({planned_user_events: $planned_user_events})
    }

}

const searchEvent = function ($planned_user_events, i) {


}

/**
 * insret event to virew list
 * @param arrvents_del
 * @param event
 */
const insertEvent = function (arr, event) {
    arr.push(event)
    arr.sort(function (a, b) {
        return a.expected_at < b.expected_at ? -1 : 1;
    })
}


const removeOnServer = function (id, ids) {
    let  remove_all=""
    if (ids) { remove_all='  ids:\"'+ids+'\"' }
    const query = "{ca { pod_events{del( pod_event_id:" + id + "  "+remove_all+" ) {  id }}}}"
    return runQuery(query);
}
