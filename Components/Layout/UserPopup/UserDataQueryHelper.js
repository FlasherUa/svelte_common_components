/**
 * loads data for user stats
 * @param userId
 * @param fetcher
 */
export default function (user_id) {
    return "{ca {users "
        + "{profile( _id:\""
        + "user_block" + user_id
        + "\" id:"
        + user_id
        + " ) { avatar id bottle_id created_at updated_at first_name last_name email phone reg_location gender age height weight goal_1 times_cardio times_strength "
        + "  intensity goal_2 top_sports  min_cardio min_strength rating rating_week sign_in_method}}"
        + "  stats{user_usage(   id:" + user_id + " ) {   items { key value   } }}"
        + "}}";
}
