/**
 * loads data for user stats
 * @param userId
 * @param fetcher
 */
export default function (user_id: number | string): string;
