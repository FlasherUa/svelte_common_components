import {debounce} from "../../../Modules/Helpers/Data/_";
import {getOffset} from "../../../Modules/Helpers/viewHelpers/projectViewHelpers"
import {getStore} from "../../../Modules/Helpers/viewHelpers/projectViewHelpers";

interface Ioffset {
    left: number,
    top: number
}

let doNotHide: Boolean = false

export function show(e: Event, data: any, top: number, left: number) {
    const el: any = e.currentTarget,
        offset: Ioffset = getOffset(el),
        popupStyle = "display:block; opacity:1; top: " + (offset.top + top) + ";left: " + (offset.left + left);
    getStore().set({_userPopupStyle: popupStyle, _userPopupData: data})
}

export function preventHide() {
    _hide("CLEAR")
}

export function allowHide() {
    _hide()
}

export function hide() {
    _hide()
}

const _hide: Function = debounce(function () {
    const popupStyle = getStore().get()._userPopupStyle
    if (popupStyle) {
        const popupStyleX = popupStyle.replace("opacity:1;", "opacity:0;")

        getStore().set({_userPopupStyle: popupStyleX})
    }
}, 500)
