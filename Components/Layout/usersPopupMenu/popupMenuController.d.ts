export declare function show(e: Event, data: any, top: number, left: number): void;
export declare function preventHide(): void;
export declare function allowHide(): void;
export declare function hide(): void;
