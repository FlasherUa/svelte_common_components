export declare class I_cropperConfig {
    width: number;
    height: number;
    callback(v: string): void;
}
