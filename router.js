import { get } from 'svelte/store';
import { hashToObject, objectToHash } from "./Modules/Helpers/Router/RouterHelper";
//@ts-ignore
import { route, defaultRoute, routeParams } from './CommonStore';
//*let route: any, defaultRoute: any, routeParams: any;
/** depency injections for Store */
/*export const init = function (route0: any, defaultRoute0: any, routeParams0: any) {
    route = route0;
    defaultRoute = defaultRoute0;
    routeParams = routeParams0;
}*/
//let route:any,defaultRoute:any,routeParams:any
// @ts-ignore
import historyFactory from "./history";
let unlisten; // @ts-ignore
let location, history;
/**
 * Checks if current route
 * @param pattern
 * @param routeObj
 * @param curStep
 * @returns {boolean}
 */
export const testRoute = function (pattern) {
    const rout = get(route);
    //simple string test
    if (pattern.toLowerCase() === rout.toLowerCase()) {
        //save search variables
        //@ts-ignore
        ////       routeParams.set({search: saveSearchParams()})
        return true;
    }
    if (testRegExp(pattern, rout)) {
        const params = parseReg(pattern, rout);
        //store location search - TODO
        const search = saveSearchParams();
        //@ts-ignore
        ////   routeParams.set({params, search})
        return true;
    }
    return false;
};
const saveSearchParams = function () {
    const search = getQueryStringParams(window.location.search);
    // window.location.search=""
    //@ts-ignore
    return search;
}, testRegExp = function (pattern, route) {
    const reg = _getRegExp(pattern);
    if (!reg)
        return false;
    else
        return reg.test(route);
}, parseReg = function (pattern, route) {
    _regExps[pattern].exec(route);
    return _regExps[pattern].exec(route);
}, _regExps = {}, _prepareRegExp = function (route) {
    let out = route.replace(":num:", "([0-9]{1,})");
    out = out.replace(":aplha:", "([a-z]{1,})");
    out = out.replace(":name:", "([a-z0-9-_,]{1,})");
    out = out.replace("\/", "\\\/");
    return new RegExp(out, "gi");
}, _getRegExp = function (route) {
    if (_regExps[route])
        return _regExps[route];
    if (route.indexOf(":") !== -1 || route.indexOf("(") !== -1) {
        _regExps[route] = _prepareRegExp(route);
    }
    else {
        _regExps[route] = false;
    }
    return _regExps[route];
};
export const getCurrentRoute = () => {
    return { route: get(route), routeParams: get(routeParams) };
};
export const isActive = function (link, $route0) {
    return link === get(route);
};
/**
 *
 * @param {string} route
 *
 * silent {Bool} just change hash, no route changes
 */
let __silent = false;
export function go(route, params = null, silent = false) {
    //@ts-ignore
    if (params)
        route = objectToHash(route, params);
    if (silent)
        __silent = true;
    //@ts-ignore
    window.location.hash = "#" + route;
}
function _onHistoryState(location) {
    if (__silent) {
        __silent = false;
        return;
    }
    const r = hashToObject(location);
    route.set(r.route);
    routeParams.set(r.params);
}
export function start(route0, defaultRoute0, routeParams0) {
    //  route=route0;  defaultRoute=defaultRoute0; routeParams=routeParams0
    stop();
    history = historyFactory(_onHistoryState);
    // Get the current location
    if (history.location == "") {
        go(get(defaultRoute));
    }
    else {
        _onHistoryState(history.location);
    }
}
export function stop() {
    if (history)
        history.stop();
}
const getQueryStringParams = (query) => {
    return query
        ? (/^[?#]/.test(query) ? query.slice(1) : query)
            .split('&')
            .reduce((params, param) => {
            let [key, value] = param.split('=');
            params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
            return params;
        }, {})
        : {};
};
