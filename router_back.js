
import {get} from 'svelte/store'
import {route,defaultRoute} from "../Dashboard/plant/MainStore"
import historyFactory from "./history"

let unlisten;
let location, history


/**
 * Checks if current route
 * @param pattern
 * @param routeObj
 * @param curStep
 * @returns {boolean}
 */
export const testRoute = function (pattern, routeObj, curStep) {
    if (pattern.toLowerCase() === get(route).toLowerCase()) return true
    return false
}

export const isActive = function (routePart, $route) {
    return routePart === $route
};

/**
 *
 * @param {string} route
 */
export function go(route, silent) {
    //if (getCurrentRoute() != route) {
        //setStateForRoute(route);
        if (!silent) history.go(route);
    //}
}
function _onHistoryState (location) {
    // location is an object like window.location
    route.set(location)
}

export function start() {
    history = historyFactory(_onHistoryState);
    stop()
    // Get the current location

    if (history.location==""){
       go(get(defaultRoute))
    }else {
        _onHistoryState (history.location)
    }
}

export function stop() {
    if (history) history.stop()
}