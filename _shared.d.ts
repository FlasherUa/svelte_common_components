/**
 * links for chared modules
 */
declare type _Tstorage = "cfg" | "Loads" | "fetcher" | "";
declare const _default: (key: _Tstorage) => any;
export default _default;
export declare const set: (key: _Tstorage, value: any) => void;
export declare let config: any;
export declare let $config: any;
export declare let route: any;
export declare let Loads: any;
export declare let fetcher: any;
