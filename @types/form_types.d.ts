export declare interface iOption {
    title:string,
    value:string|any
}

export declare interface IFormItemInterface {
    value:any|Promise<any>,
    title:string,
    hint:string,
    options:IFormItemOptions,
    errors: IFormItemError[],
    state:TFormState
}

export declare interface IFormItemConfig {
    title:string,
    hint:string,
    options:IFormItemOptions,
    //errors: IFormItemError[],
    //state:TFormState
    getValue?:Function,
    setValue?:Function,
    _value?:any // inner representation of value
}




export  type TFormState = null | "beforeLoad"| "loading"



declare interface IFormItemError {
    code?:number,
    message:string
}

//to be extended by types interfaces
declare interface IFormItemOptions {

}
export declare type uId =number
