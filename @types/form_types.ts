export interface iOption {
    title:string,
    value:string|any
}

export  interface IFormItemInterface {
    value:any|Promise<any>,
    title:string,
    hint:string,
    options:IFormItemOptions,
    errors: IFormItemError[],
    state:TFormState
}

export  interface IFormItemConfig {
    title:string,
    hint:string,
    options:IFormItemOptions,
    //errors: IFormItemError[],
    //state:TFormState
    getValue?:Function,
    setValue?:Function,
    _value?:any // inner representation of value
}




//export  type TFormState = null | "beforeLoad"| "loading"


export  interface I_FormItemError {
    code?:number,
    message:string
}

export class IFormItemError implements I_FormItemError {
    message=""
}

//to be extended by types interfaces
export class IFormItemOptions {

}
export class TFormState {

}
export declare type uId =number
