export declare type TrouteParams = {
    route: string;
    search?: any;
    params?: any;
};
export declare type iLoadComponent = {
    reload: Function;
};
