/**
 * Main store - do not edit
 */
import {writable, get} from 'svelte/store';
//import {writable} from 'svelte/store';

import {getSessionUser} from "./Modules/Helpers/userSession"

/** */
export const svelte2state = writable ({});

/** */
export const config = writable ({});

/** Current rout (address string)*/
export const routeParams = writable ({});

/** Current route parsed*/
export const route = writable ('manage');

/** */
export const user = writable (getSessionUser());

/** */
export const popup = writable (false);

/** */
export const notification = writable ({show:false});

/** */
export const mobile_mode = writable (false);

/** debug function */
export const d = writable (function (v) {
    console.log(v)
    return stringify(v)
});

/** */
export const dd = writable (function (v) {
    console.log(v)
    debugger
});

/** Current route parsed*/
export const defaultRoute = writable ('manage');

/** Current rout (address string)*/
export const routeStr = writable ('');

/** */
export const pod_types = writable (false);

/** */
export const plan_favorites = writable (false);

/** */
export const pod_types_byId = writable (false);

/** */
export const plan_favorites_byId = writable (false);

export const appFilterCfg =writable (false) ;


